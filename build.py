#   -*- coding: utf-8 -*-
from pybuilder.core import use_plugin, init, task

use_plugin("python.core")
use_plugin("python.distutils")


name = "pybuilder-plugin-demo"
default_task = "publish"


@init
def set_properties(project):
    project.set_property("property_for_my_plugin", 42)
