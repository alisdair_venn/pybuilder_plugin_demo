from pybuilder.core import task


@task("foo", description="Greets people")
def hello(project):
    print(f"hello {project.get_property('property_for_my_plugin')}")